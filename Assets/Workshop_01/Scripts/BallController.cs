using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    private Vector3 startingPosition;
    private Rigidbody body;

    void Start()
    {
        startingPosition = gameObject.transform.position;
        body = gameObject.GetComponent<Rigidbody>();
        stopAndBackToStartingPosition();
    }

    void Update()
    {
        // Rozpocz�cie ruchu pi�ki.
        // Lewitacja.
    }

    void FixedUpdate()
    {
        // Maksymalny opad pi�ki i restart.
        // Obs�uga zapytania raycast.
    }

    private void stopAndBackToStartingPosition()
    {
        // Unieruchomienie obiektu i powr�t do pozycji pocz�tkowej.
    }

    private void OnTriggerEnter(Collider other)
    {
        // Obs�uga pu�apki grawitacyjnej.
    }
}
